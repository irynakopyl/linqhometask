﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using Newtonsoft.Json;
using System.Web;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Reflection.Metadata.Ecma335;

namespace LinqHomework
{
    class Program
    {
        static void Main(string[] args)
        {
            string link = "https://bsa20.azurewebsites.net/api/";
            Console.WriteLine("Hello World!");
            //getting data
            List<Project> projects = GetAllProjects(link + "Projects").Result;
            List<User> users = GetAllUsers(link + "Users").Result;
            List<Task> tasks = GetAllTasks(link + "Tasks").Result;
            List<Team> teams = GetAllTeams(link + "Teams").Result;
            //mapping entities
            //getting performer
            tasks = tasks.Join(
                users,
                t => t.PerformerId,
                x => x.Id,
                (task, performer) => new Task
                {
                    Id = task.Id,
                    Name = task.Name,
                    Description = task.Description,
                    CreatedAt = task.CreatedAt,
                    FinishedAt = task.FinishedAt,
                    TaskState = task.TaskState,
                    ProjectId = task.ProjectId,
                    PerformerId = task.PerformerId,
                    Performer = performer
                }
                ).ToList();
            //getting Author
            projects = projects.Join(
                users,
                t => t.AuthorId,
                x => x.Id,
                (proj, author) => new Project
                {
                    Id = proj.Id,
                    Name = proj.Name,
                    Description = proj.Description,
                    CreatedAt = proj.CreatedAt,
                    Deadline = proj.Deadline,
                    AuthorId = proj.AuthorId,
                    TeamId = proj.TeamId,
                    Team = proj.Team,
                    Tasks = proj.Tasks,
                    Author = author,
                }
                ).ToList().Join(
                    teams,
                    p => p.TeamId,
                    t => t.Id,
                    (proj, team) => new Project
                    {
                        Id = proj.Id,
                        Name = proj.Name,
                        Description = proj.Description,
                        CreatedAt = proj.CreatedAt,
                        Deadline = proj.Deadline,
                        AuthorId = proj.AuthorId,
                        TeamId = proj.TeamId,
                        Author = proj.Author,
                        Team = team,
                        Tasks = proj.Tasks
                    }
                ).ToList().GroupJoin(
                tasks,
                p => p.Id,
                t => t.ProjectId,
                (proj, task) => new Project()
                {
                    Id = proj.Id,
                    Name = proj.Name,
                    Description = proj.Description,
                    CreatedAt = proj.CreatedAt,
                    Deadline = proj.Deadline,
                    AuthorId = proj.AuthorId,
                    TeamId = proj.TeamId,
                    Author = proj.Author,
                    Team = proj.Team,
                    Tasks = task.ToList()
                }
                ).ToList();
            Console.WriteLine(users.Count);

            //task1
            Console.WriteLine("\n--------- Let's check the 1st task! ---------");
            Console.WriteLine("********** It is user with id 48 *********");
            foreach (KeyValuePair<Project, int> keyValue in GetTaskAmountInProjects(projects, 48))
            {
                Console.WriteLine("It is project with name: "+keyValue.Key.Name + " and User has " + keyValue.Value +" tasks in it");
            }
            //task2
            Console.WriteLine("\n--------- Let's check the 2nd task! ---------");
            Console.WriteLine("********** It is user with id 48 *********");
            foreach (Task u in GetAllTasksWithNameLessThan45Chars(projects, 48))
            {
                Console.WriteLine("I am task for user48 and I have less than 45 digits in ma name: "+u.Name);
            }
            //task3
            Console.WriteLine("\n--------- Let's check the 3rd task! ---------");
            foreach (ReadyTaskInCurrentYear u in GetAllTasksFinishedInCurrentYear(projects, 48))
            {
                Console.WriteLine("I am task ready in curr year: " + u.Name);
            }
            //task4
            Console.WriteLine("\n--------- Let's check the 4th task! ---------");
            foreach (TeamList u in GetTeamsWithUsersOlderThan10(projects))
            {
                Console.WriteLine("\n It is a team with Id: " + u.Id + " Its Name is: " + u.TeamName + " ---Team Members who are older than 10 are: ");
                foreach (User l in u.Members)
                {
                    Console.WriteLine("\tMem:  " + l.Id + " --- " + l.FirstName + " --- " + l.RegisteredAt + " Team Id " + l.TeamId);
                }
            }
            //task5
            Console.WriteLine("\n--------- Let's check the 5th task! ---------");
            Console.WriteLine(GetAllSortedUsersWithSortedTasks(projects).Count);
            foreach (TasksForUserList u in GetAllSortedUsersWithSortedTasks(projects))
            {
                Console.WriteLine("\n I am a performer of this tasks and my Name is: " + u.Performer.FirstName + "\t" + u.Performer.LastName);
                foreach (Task t in u.Tasks)
                {
                    Console.WriteLine("\tIt is a task  " + t.Name + "  with Id" + t.Id + " for a user with Id" + t.PerformerId);
                }
            }
            //task6
            UserInfo uzzer = GetTasksAndProjectsInfo(projects, 8);
            Console.WriteLine("\n--------- Let's check the 6th task! ---------");
            Console.WriteLine("I am user with id: "+ uzzer.User.Id+ " My last project is: "+ uzzer.LastProject.Name+" My longest task is "+ uzzer.LongestTask + "I have " + uzzer.NotReadyTasks +" not ready tasks");

            //task7
            Console.WriteLine("\n--------- Let's check the 7th task! ---------");
            foreach (ProjectInfo u in GetAllProjectsAndItsTasksInfo(projects))
            {
                Console.WriteLine("\n It is a project with id: " + u.Proj.Id + "\t Its longest task is" + u.LongestTask.Name + "\t Its shortest task is:"+u.ShortestTask.Name
                    + "\t Users count:" + u.UserAmount);
                
            }
            
        }

        static async Task<List<Project>> GetAllProjects(string link)
        {
            using HttpClient httpClient = new HttpClient();
            string responseBody = await
                (await httpClient.GetAsync(link)).
                EnsureSuccessStatusCode().
                Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<Project>>(responseBody);
        }
        static async Task<List<User>> GetAllUsers(string link)
        {
            using HttpClient httpClient = new HttpClient();
            string responseBody = await
                (await httpClient.GetAsync(link)).
                EnsureSuccessStatusCode().
                Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<User>>(responseBody);
        }
        static async Task<List<Task>> GetAllTasks(string link)
        {
            using HttpClient httpClient = new HttpClient();
            string responseBody = await
                (await httpClient.GetAsync(link)).
                EnsureSuccessStatusCode().
                Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<Task>>(responseBody);
        }
        static async Task<List<Team>> GetAllTeams(string link)
        {
            using HttpClient httpClient = new HttpClient();
            string responseBody = await
                (await httpClient.GetAsync(link)).
                EnsureSuccessStatusCode().
                Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<Team>>(responseBody);
        }
        //task1
        static Dictionary<Project, int> GetTaskAmountInProjects(List<Project> projects, int userId)
        {
            return projects.Select(
                p => (p, p.Tasks.Where(
                    x => x.PerformerId == userId).Count())).ToDictionary(p => p.p, p => p.Item2);
        }
        //task2
        static List<Task> GetAllTasksWithNameLessThan45Chars(List<Project> projects, int userId)
        {
            return projects.SelectMany(
                p => p.Tasks.Where(
                    x => x.PerformerId == userId && x.Name.Length < 45)).
                    ToList();
        }
        //task3  Отримати список(id, name) з колекції тасків, які виконані(finished) в поточному(2020) році для конкретного користувача(по id).
        static List<ReadyTaskInCurrentYear> GetAllTasksFinishedInCurrentYear(List<Project> projects, int userId)
        {
            return projects.SelectMany(
                p => p.Tasks.Where(x => x.TaskState == TaskState.Finished && x.FinishedAt.Year == 2020 && x.PerformerId == userId))
                .Select(t =>
               new ReadyTaskInCurrentYear
               {
                   Id = t.Id,
                   Name = t.Name
               }).ToList();
        }
        //task4
        static List<TeamList> GetTeamsWithUsersOlderThan10(List<Project> projects)
        {
            return projects.SelectMany(p => p.Tasks.Select(t => t.Performer).Where(u => (2020 - u.Birthday.Year) > 10)).OrderByDescending(u => u.RegisteredAt).Distinct().GroupBy(x => x.TeamId)
                .Select(x => new TeamList
                {
                    Id = x.Key,
                    Members = x.ToList()
                }
                ).Join(
                projects.Select(p => p.Team).Distinct(),
                 team => team.Id,
                 user => user.Id,
                (t, u) =>
                new TeamList
                {
                    Id = t.Id,
                    TeamName = u.Name,
                    Members = t.Members
                }).ToList();
        }
        //task 5
        static List<TasksForUserList> GetAllSortedUsersWithSortedTasks(List<Project> projects)
        {
            return projects.SelectMany(p => p.Tasks)
                .Distinct()
                .OrderByDescending(t => t.Name.Length)
                .GroupBy(p => p.Performer).Select(
                 x => new TasksForUserList
                 {
                     Performer = x.Key,
                     Tasks = x.ToList()
                 }
                 ).OrderBy(t => t.Performer.FirstName).ToList();
        }
        //task6 - не сделан в один запрос, но наверное лучше сделать хоть как то чем не сделать совсем
        static UserInfo GetTasksAndProjectsInfo(List<Project> projects, int userId)
        {
            UserInfo user = new UserInfo() { };
            user.LastProject = projects.Where(p => p.AuthorId == userId).OrderByDescending(p => p.CreatedAt).First();//* Останній проект користувача(за датою створення)
            user.TaskAmountUnderLastProject = user.LastProject.Tasks.Count;//* Загальна кількість тасків під останнім проектом
            user.User = user.LastProject.Author;

            /*.Select(x=>new Task6 { LastProject = x, TaskAmountUnderLastProject = x.Tasks.Count}); */

            user.LongestTask = projects.SelectMany(p => p.Tasks.Where(p => p.PerformerId == userId)) //* Найтриваліший таск користувача за датою(найраніше створений - найпізніше закінчений)
               .Distinct()
               .OrderByDescending(t => t.FinishedAt - t.CreatedAt).First();

            user.NotReadyTasks = projects.SelectMany(p => p.Tasks.Where(
                p => (p.PerformerId == userId && (p.TaskState == TaskState.Canceled || p.TaskState == TaskState.Started)))).Distinct().Count();//* Загальна кількість незавершених або скасованих тасків для користувача
            return user;
            
        }
        //task7 
        static List<ProjectInfo> GetAllProjectsAndItsTasksInfo(List<Project> projects)
        {
            return projects.SelectMany(p => p.Tasks.Select(t => t.Performer)).Distinct().GroupBy(x => x.TeamId)
            .Select(x => new TeamINNN
            {
                Id = x.Key,
                Members = x.Count()
            }
            ).Join(
            projects.Where(p => p.Description.Length > 20 || p.Tasks.Count() < 3),
                 team => team.Id,
                 proj => proj.TeamId,
                (t, p) =>
                new ProjectInfo
                {
                    Proj = p,
                    LongestTask = p.Tasks.OrderByDescending(p => p.Description).First(),
                    ShortestTask = p.Tasks.OrderBy(p => p.Name).First(),
                    UserAmount = t.Members
                }).ToList();
        }
    }
    class TeamINNN
    {
        public int? Id { get; set; }
        public int Members { get; set; }
    }
    class ProjectInfo
    {
        public Project Proj { get; set; }
        public Task LongestTask { get; set; }
        public Task ShortestTask { get; set; }
        public int UserAmount { get; set; }
    }
    class ReadyTaskInCurrentYear
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    class TeamList
    {
        public int? Id { get; set; }
        public string TeamName { get; set; }
        public List<User> Members { get; set; }
    }
    class TasksForUserList
    {
        public User Performer { get; set; }
        public List<Task> Tasks { get; set; }
    }
    class UserInfo
    {
        public User User { get; set; }
        public Project LastProject { get; set; }
        public int TaskAmountUnderLastProject { get; set; }
        public int NotReadyTasks { get; set; }
        public Task LongestTask { get; set; }

    }

    class Project
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }
        [JsonProperty("deadline")]
        public DateTime Deadline { get; set; }
        [JsonProperty("authorId")]
        public int AuthorId { get; set; }
        [JsonProperty("teamId")]
        public int TeamId { get; set; }

        public List<Task> Tasks { get; set; }
        public User Author { get; set; }
        public Team Team { get; set; }
    }
    class Task
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }
        [JsonProperty("finishedAt")]
        public DateTime FinishedAt { get; set; }
        [JsonProperty("state")]
        public TaskState TaskState { get; set; }

        [JsonProperty("projectId")]
        public int ProjectId { get; set; }

        [JsonProperty("performerId")]
        public int PerformerId { get; set; }

        public User Performer { get; set; }
    }
    enum TaskState
    {
        Created = 0,
        Started = 1,
        Finished = 2,
        Canceled = 3
    }
    class Team
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }

    }
    class User
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("firstName")]
        public string FirstName { get; set; }
        [JsonProperty("lastName")]
        public string LastName { get; set; }
        [JsonProperty("createdAt")]
        public string Email { get; set; }
        [JsonProperty("birthday")]
        public DateTime Birthday { get; set; }
        [JsonProperty("registeredAt")]
        public DateTime RegisteredAt { get; set; }
        [JsonProperty("teamId")]
        public int? TeamId { get; set; }
    }

}


